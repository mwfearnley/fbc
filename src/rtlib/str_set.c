/* lset and rset functions */

#include "fb.h"
/* lset should be a straight assignment for fixed-length strings */

FBCALL void fb_StrLset ( FBSTRING *dst, FBSTRING *src )
{
	ssize_t slen, dlen, len;

	if( (dst != NULL) && (dst->data != NULL) && (src != NULL) )
	{
		slen = FB_STRSIZE( src );
		dlen = FB_STRSIZE( dst );

		if( dlen > 0 )
		{
			len = (dlen <= slen? dlen: slen );

			fb_hStrCopy( dst->data, src->data, len );

			len = dlen - slen;
			if( len > 0 )
			{
				memset( &dst->data[slen], ' ', len );

				/* null char */
				dst->data[slen+len] = '\0';
			}
		}
	}

	/* del if temp */
	fb_hStrDelTemp( src );

	/* del if temp */
	fb_hStrDelTemp( dst );
}

/* rset should not add a null for fixed-length strings */
FBCALL void fb_StrRset ( FBSTRING *dst, FBSTRING *src )
{
	ssize_t slen, dlen, len, padlen;

	if( (dst != NULL) && (dst->data != NULL) && (src != NULL) )
	{
		slen = FB_STRSIZE( src );
		dlen = FB_STRSIZE( dst );

		if( dlen > 0 )
		{
			padlen = dlen - slen;
			if( padlen > 0 )
				memset( dst->data, 32, padlen );
			else
				padlen = 0;

			len = (dlen <= slen? dlen: slen );

			fb_hStrCopy( &dst->data[padlen], src->data, len );
		}
	}

	/* del if temp */
	fb_hStrDelTemp( src );

	/* del if temp */
	fb_hStrDelTemp( dst );
}
