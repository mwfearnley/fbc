#pragma once

#include once "_mingw.bi"

#define _BSDTYPES_DEFINED
type u_char as ubyte
type u_short as ushort
type u_int as ulong
type u_long as ulong
type u_int64 as ulongint
