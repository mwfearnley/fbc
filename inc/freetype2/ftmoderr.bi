#pragma once

#define __FTMODERR_H__

enum
	FT_Mod_Err_Base = 0
	FT_Mod_Err_Autofit = 0
	FT_Mod_Err_BDF = 0
	FT_Mod_Err_Bzip2 = 0
	FT_Mod_Err_Cache = 0
	FT_Mod_Err_CFF = 0
	FT_Mod_Err_CID = 0
	FT_Mod_Err_Gzip = 0
	FT_Mod_Err_LZW = 0
	FT_Mod_Err_OTvalid = 0
	FT_Mod_Err_PCF = 0
	FT_Mod_Err_PFR = 0
	FT_Mod_Err_PSaux = 0
	FT_Mod_Err_PShinter = 0
	FT_Mod_Err_PSnames = 0
	FT_Mod_Err_Raster = 0
	FT_Mod_Err_SFNT = 0
	FT_Mod_Err_Smooth = 0
	FT_Mod_Err_TrueType = 0
	FT_Mod_Err_Type1 = 0
	FT_Mod_Err_Type42 = 0
	FT_Mod_Err_Winfonts = 0
	FT_Mod_Err_GXvalid = 0
	FT_Mod_Err_Max
end enum
