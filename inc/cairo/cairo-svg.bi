#pragma once

#include once "cairo.bi"

extern "C"

#define CAIRO_SVG_H

type _cairo_svg_version as long
enum
	CAIRO_SVG_VERSION_1_1
	CAIRO_SVG_VERSION_1_2
end enum

type cairo_svg_version_t as _cairo_svg_version
declare function cairo_svg_surface_create(byval filename as const zstring ptr, byval width_in_points as double, byval height_in_points as double) as cairo_surface_t ptr
declare function cairo_svg_surface_create_for_stream(byval write_func as cairo_write_func_t, byval closure as any ptr, byval width_in_points as double, byval height_in_points as double) as cairo_surface_t ptr
declare sub cairo_svg_surface_restrict_to_version(byval surface as cairo_surface_t ptr, byval version as cairo_svg_version_t)
declare sub cairo_svg_get_versions(byval versions as const cairo_svg_version_t ptr ptr, byval num_versions as long ptr)
declare function cairo_svg_version_to_string(byval version as cairo_svg_version_t) as const zstring ptr

end extern
