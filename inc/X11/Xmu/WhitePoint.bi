#pragma once

const CIE_A_u = 0.2560
const CIE_A_v = 0.5243
const CIE_A_Y = 1.0000
const CIE_B_u = 0.2137
const CIE_B_v = 0.4852
const CIE_B_Y = 1.0000
const CIE_C_u = 0.2009
const CIE_C_v = 0.4609
const CIE_C_Y = 1.0000
const CIE_D55_u = 0.2044
const CIE_D55_v = 0.4808
const CIE_D55_Y = 1.0000
const CIE_D65_u = 0.1978
const CIE_D65_v = 0.4684
const CIE_D65_Y = 1.0000
const CIE_D75_u = 0.1935
const CIE_D75_v = 0.4586
const CIE_D75_Y = 1.0000
const ASTM_D50_u = 0.2092
const ASTM_D50_v = 0.4881
const ASTM_D50_Y = 1.0000
const WP_9300K_u = 0.1884
const WP_9300K_v = 0.4463
const WP_9300K_Y = 1.0000
