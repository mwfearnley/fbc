#pragma once

#define _XF86DGA1CONST_H_
const X_XF86DGAQueryVersion = 0
const X_XF86DGAGetVideoLL = 1
const X_XF86DGADirectVideo = 2
const X_XF86DGAGetViewPortSize = 3
const X_XF86DGASetViewPort = 4
const X_XF86DGAGetVidPage = 5
const X_XF86DGASetVidPage = 6
const X_XF86DGAInstallColormap = 7
const X_XF86DGAQueryDirectVideo = 8
const X_XF86DGAViewPortChanged = 9
const XF86DGADirectPresent = &h0001
const XF86DGADirectGraphics = &h0002
const XF86DGADirectMouse = &h0004
const XF86DGADirectKeyb = &h0008
const XF86DGAHasColormap = &h0100
const XF86DGADirectColormap = &h0200
