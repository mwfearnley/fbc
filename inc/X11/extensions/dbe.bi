#pragma once

#define DBE_H
const XdbeUndefined = 0
const XdbeBackground = 1
const XdbeUntouched = 2
const XdbeCopied = 3
const XdbeBadBuffer = 0
#define DBE_PROTOCOL_NAME "DOUBLE-BUFFER"
const DBE_MAJOR_VERSION = 1
const DBE_MINOR_VERSION = 0
const DbeNumberEvents = 0
const DbeBadBuffer = 0
#define DbeNumberErrors (DbeBadBuffer + 1)
