#pragma once

#define _EVI_H_
const XEVI_TRANSPARENCY_NONE = 0
const XEVI_TRANSPARENCY_PIXEL = 1
const XEVI_TRANSPARENCY_MASK = 2
#define EVINAME "Extended-Visual-Information"
const XEVI_MAJOR_VERSION = 1
const XEVI_MINOR_VERSION = 0
