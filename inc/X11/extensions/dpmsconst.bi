#pragma once

const _DPMSCONST_H = 1
const DPMSMajorVersion = 1
const DPMSMinorVersion = 1
#define DPMSExtensionName "DPMS"
const DPMSModeOn = 0
const DPMSModeStandby = 1
const DPMSModeSuspend = 2
const DPMSModeOff = 3
