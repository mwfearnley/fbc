#pragma once

#define _GE_H_
#define GE_NAME "Generic Event Extension"
const GE_MAJOR = 1
const GE_MINOR = 0
const X_GEQueryVersion = 0
#define GENumberRequests (X_GEQueryVersion + 1)
const GENumberEvents = 0
const GENumberErrors = 0
