#pragma once

#define _XTEST_CONST_H_
const XTestNumberEvents = 0
const XTestNumberErrors = 0
#define XTestCurrentCursor cast(Cursor, 1)
const XTestMajorVersion = 2
const XTestMinorVersion = 2
#define XTestExtensionName "XTEST"
