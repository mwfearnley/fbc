#pragma once

#define _ICE_H_
const IceProtoMajor = 1
const IceProtoMinor = 0
const IceLSBfirst = 0
const IceMSBfirst = 1
const ICE_Error = 0
const ICE_ByteOrder = 1
const ICE_ConnectionSetup = 2
const ICE_AuthRequired = 3
const ICE_AuthReply = 4
const ICE_AuthNextPhase = 5
const ICE_ConnectionReply = 6
const ICE_ProtocolSetup = 7
const ICE_ProtocolReply = 8
const ICE_Ping = 9
const ICE_PingReply = 10
const ICE_WantToClose = 11
const ICE_NoClose = 12
const IceCanContinue = 0
const IceFatalToProtocol = 1
const IceFatalToConnection = 2
const IceBadMinor = &h8000
const IceBadState = &h8001
const IceBadLength = &h8002
const IceBadValue = &h8003
const IceBadMajor = 0
const IceNoAuth = 1
const IceNoVersion = 2
const IceSetupFailed = 3
const IceAuthRejected = 4
const IceAuthFailed = 5
const IceProtocolDuplicate = 6
const IceMajorOpcodeDuplicate = 7
const IceUnknownProtocol = 8
