#pragma once

const apXK_LineDel = &h1000FF00
const apXK_CharDel = &h1000FF01
const apXK_Copy = &h1000FF02
const apXK_Cut = &h1000FF03
const apXK_Paste = &h1000FF04
const apXK_Move = &h1000FF05
const apXK_Grow = &h1000FF06
const apXK_Cmd = &h1000FF07
const apXK_Shell = &h1000FF08
const apXK_LeftBar = &h1000FF09
const apXK_RightBar = &h1000FF0A
const apXK_LeftBox = &h1000FF0B
const apXK_RightBox = &h1000FF0C
const apXK_UpBox = &h1000FF0D
const apXK_DownBox = &h1000FF0E
const apXK_Pop = &h1000FF0F
const apXK_Read = &h1000FF10
const apXK_Edit = &h1000FF11
const apXK_Save = &h1000FF12
const apXK_Exit = &h1000FF13
const apXK_Repeat = &h1000FF14
const apXK_KP_parenleft = &h1000FFA8
const apXK_KP_parenright = &h1000FFA9
