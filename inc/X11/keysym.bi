#pragma once

#include once "X11/keysymdef.bi"

'' The following symbols have been renamed:
''     #define XK_KATAKANA => XK_KATAKANA_
''     #define XK_CURRENCY => XK_CURRENCY_

#define XK_MISCELLANY
#define XK_XKB_KEYS
#define XK_LATIN1
#define XK_LATIN2
#define XK_LATIN3
#define XK_LATIN4
#define XK_LATIN8
#define XK_LATIN9
#define XK_CAUCASUS
#define XK_GREEK
#define XK_KATAKANA_
#define XK_ARABIC
#define XK_CYRILLIC
#define XK_HEBREW
#define XK_THAI
#define XK_KOREAN
#define XK_ARMENIAN
#define XK_GEORGIAN
#define XK_VIETNAMESE
#define XK_CURRENCY_
#define XK_MATHEMATICAL
#define XK_BRAILLE
#define XK_SINHALA
