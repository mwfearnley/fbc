#pragma once

const DXK_ring_accent = &h1000FEB0
const DXK_circumflex_accent = &h1000FE5E
const DXK_cedilla_accent = &h1000FE2C
const DXK_acute_accent = &h1000FE27
const DXK_grave_accent = &h1000FE60
const DXK_tilde = &h1000FE7E
const DXK_diaeresis = &h1000FE22
const DXK_Remove = &h1000FF00
