#pragma once

#define _XFUNCPROTO_H_
const NeedFunctionPrototypes = 1
const NeedVarargsPrototypes = 1
const NeedNestedPrototypes = 1
const NARROWPROTO = 1
const NeedWidePrototypes = 0
